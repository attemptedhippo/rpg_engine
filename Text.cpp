#include "Text.h"

Text::Text() {

}

std::string Text::Int2String(int i) {
	std::stringstream sstream;

	sstream << i;

	return sstream.str();
}

std::string Text::Float2String(float i) {
	std::stringstream sstream;

	sstream << i;

	return sstream.str();
}

TTF_Font* Text::LoadFont(const std::string &fontFile, int fontSize) {
	TTF_Font *font = TTF_OpenFont(fontFile.c_str(), fontSize);
	if (font == nullptr) {
		std::cout << "LoadFont Error : " << SDL_GetError() << std::endl;
		return nullptr;
	}
	return font;
}
