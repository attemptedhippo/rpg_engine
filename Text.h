#pragma once

#include <iostream>
#include <sstream>
#include <string>
#include <vector>
#include <SDL_ttf.h>

const SDL_Color clrWHITE = { 255, 255, 255 };
const SDL_Color clrGREY = { 255 / 2, 255 / 2, 255 / 2 };
const SDL_Color clrBLACK = { 0, 0, 0 };
const SDL_Color clrRED = { 255, 0, 0 };
const SDL_Color clrGREEN = { 0, 255, 0 };
const SDL_Color clrBLUE = { 0, 0, 128 };
const SDL_Color clrYELLOW = { 255, 255, 0 };
const SDL_Color clrALPHA = { 255, 0, 255 };
const SDL_Color clrTILE_FG = { 0, 64, 0 };
const SDL_Color clrTILE_BG = { 0, 0, 128 };
const SDL_Color clrTHE_VOID = { 96, 0, 0 };

class Text {
public:
	Text();
	static std::string Int2String(int i);
	static std::string Float2String(float i);
	static TTF_Font*   LoadFont(const std::string& fntName, int fntSize);
};