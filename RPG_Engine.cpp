#include <iostream>
#include <sstream>
#include <string>
#include "depends.h"
#include "FPS.h"
#include "Text.h"

const int SCREEN_WIDTH = 735;
const int SCREEN_HEIGHT = 950;

void logSDLError(std::ostream &os, const std::string &msg) {
	os << msg << " error : " << SDL_GetError() << std::endl;
}

SDL_Texture* loadTexture(const std::string &file, SDL_Renderer *ren) {
	SDL_Texture *texture = IMG_LoadTexture(ren, file.c_str());
	if (texture == nullptr) {
		logSDLError(std::cout, "LoadTexture");
	}
	return texture;
}

SDL_Texture* renderText(const std::string &message, TTF_Font *font, SDL_Color color, SDL_Renderer *ren) {
	SDL_Surface *surf = TTF_RenderText_Blended(font, message.c_str(), color);
	if (surf == nullptr) {
		logSDLError(std::cout, "TTF_RenderText");
		return nullptr;
	}
	SDL_Texture *texture = SDL_CreateTextureFromSurface(ren, surf);
	if (texture == nullptr) {
		logSDLError(std::cout, "CreateTexture");
	}
	SDL_FreeSurface(surf);
	return texture;
}

void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, SDL_Rect dst, SDL_Rect *clip = nullptr) {
	SDL_RenderCopy(ren, tex, clip, &dst);
}

void renderTexture(SDL_Texture *tex, SDL_Renderer *ren, int x, int y, SDL_Rect *clip = nullptr) {
	SDL_Rect dst;
	dst.x = x;
	dst.y = y;
	if (clip != nullptr) {
		dst.w = clip->w;
		dst.y = clip->y;
	}else {
		SDL_QueryTexture(tex, NULL, NULL, &dst.w, &dst.h);
	}
	renderTexture(tex, ren, dst, clip);
}

int main(int argc, char *argv[]) {
	std::cout << getResourcePath() << std::endl;

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0){
		logSDLError(std::cout, "SDL_Init");
		return 1;
	}

	if (TTF_Init() != 0) {
		logSDLError(std::cout, "TTF_Init");
		SDL_Quit();
		return 1;
	}

	if ((IMG_Init(IMG_INIT_PNG) & IMG_INIT_PNG) != IMG_INIT_PNG) {
		logSDLError(std::cout, "IMG_Init");
		TTF_Quit();
		SDL_Quit();
		system("pause");
		return 1;
	}

	SDL_Window *win = SDL_CreateWindow("RPG_Engine", 100, 100, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	if (win == nullptr) {
		logSDLError(std::cout, "CreateWindow");
		TTF_Quit();
		SDL_Quit();
		system("pause");
		return 1;
	}

	SDL_Renderer *ren = SDL_CreateRenderer(win, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (ren == nullptr) {
		logSDLError(std::cout, "CreateRenderer");
		cleanup(win);
		TTF_Quit();
		SDL_Quit();
		system("pause");
		return 1;
	}

	SDL_Texture *image = loadTexture(getResourcePath() + "img.png", ren);
	if (image == nullptr) {
		cleanup(image, ren, win);
		TTF_Quit();
		SDL_Quit();
		system("pause");
		return 1;
	}

	TTF_Font *font = Text::LoadFont(getResourcePath() + "debug.ttf", 10);
	if (font == nullptr) {
		cleanup(font, image, ren, win);
		TTF_Quit();
		SDL_Quit();
		system("pause");
		return 1;
	}

	SDL_Color color = { 255, 255, 255, 255 };
	SDL_Texture *text = renderText("FPS: " + Text::Int2String(FPS::FPSControl.getFPS()), font, color, ren);
	if (text == nullptr){
		cleanup(font, image, ren, win);
		TTF_Quit();
		SDL_Quit();
		return 1;
	}

	SDL_Rect rekt;
	rekt.x = 0;
	rekt.y = 0;
	rekt.w = SCREEN_WIDTH;
	rekt.h = SCREEN_HEIGHT;

	SDL_Event e;
	bool quit = false;
	while (!quit) {
		while (SDL_PollEvent(&e)) {
			if (e.type == SDL_QUIT) {
				quit = true;
			}
		}
		FPS::FPSControl.onLoop();
		text = renderText("FPS: " + Text::Int2String(FPS::FPSControl.getFPS()), font, color, ren);

		SDL_RenderClear(ren);
		renderTexture(text, ren, 0, 0);
		renderTexture(image, ren, rekt, NULL);
		SDL_RenderPresent(ren);
	}

	cleanup(image, ren, win);
	TTF_Quit();
	SDL_Quit();
	return 0;
}


