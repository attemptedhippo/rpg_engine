#pragma once
#include <SDL.h>

class FPS {
private:
	unsigned int m_oldTime;
	unsigned int m_lastTime;
	unsigned int m_numFrames;
	unsigned int m_frames;
	float m_speedFactor;
public:
	static FPS FPSControl;
	FPS();
	float getSpeedFactor();
	void onLoop();
	int getFPS();
};