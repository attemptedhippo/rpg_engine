#include "FPS.h"

FPS FPS::FPSControl;

FPS::FPS() :
m_oldTime(0), m_lastTime(0), m_frames(0), m_numFrames(0), m_speedFactor(0) {
	//lel init lists
}

void FPS::onLoop() {
	if (m_oldTime + 1000 < SDL_GetTicks()) {
		m_oldTime = SDL_GetTicks();
		m_numFrames = m_frames;
		m_frames = 0;
	}
	m_speedFactor = ((SDL_GetTicks() - m_lastTime) / 1000.0f) * 32.0f;
	m_lastTime = SDL_GetTicks();
	m_frames++;
}

int FPS::getFPS() {
	return m_numFrames;
}

float FPS::getSpeedFactor() {
	return m_speedFactor;
}